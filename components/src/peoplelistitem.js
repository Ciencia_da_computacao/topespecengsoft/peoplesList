import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CircleImage from './circleImage';


const PeopleListItem = props => {
  const { people, key } = props;
  const {picture} = people
  const { first, last, title } = people.name;

  // Colocar a primeira letra em maiusculo
  const capitalizedFirst = first[0].toUpperCase() + first.slice(1)

  let capitalizedLast = ""
  last.split(" ").map(name => capitalizedLast+= name[0].toUpperCase() + name.slice(1)+" ");

  const capitalizedTitle = title[0].toUpperCase() + title.slice(1)
  

  return (
    <View key={key} style={styles.line}>
      <CircleImage url={picture.thumbnail}/>
      <Text style={styles.lineText}>{capitalizedTitle +". "+ capitalizedFirst+ " "+ capitalizedLast}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  line: {
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#bbb',
    alignItems: 'center',
    flexDirection: 'row',
  },

  lineText: {
    fontSize: 20,
    paddingLeft: 15,
  },
});

export default PeopleListItem;
