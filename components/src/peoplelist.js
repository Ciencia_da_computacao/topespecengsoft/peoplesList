import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PeopleListItem from './peoplelistitem'

const PeopleList = props => {
    let textElements = null
    let items = null
    if (props.peoples != []) {
        const { peoples } = props;
        items = peoples.map(people => {
            console.log(people.name);

            return <PeopleListItem
                key={people.name.first}
                people={people} />
        }
        )
    }

    return (
        <View style={styles.container}>
            {items}
        </View>
    );
};

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#e2f9ff'
    },



})


export default PeopleList;