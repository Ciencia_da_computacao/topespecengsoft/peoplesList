import React from 'react'
import {Image,View, StyleSheet} from 'react-native'

export default ({url}) =>{
    console.log(url);
  return(
    // <View style={{width: 50, height: 50}}>
     <Image source={{uri:url}} style={styles.imageDefault}/>
    // </View>
  )
}

const styles = StyleSheet.create({
  imageDefault: {
    borderRadius: 50,
    width: 50,
    height: 50
  }

});

